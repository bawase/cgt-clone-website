const newChatButton = document.querySelector('.top-button:nth-child(1)');
const existingChats = document.querySelector('.existing-chats');
const inputBox = document.querySelector('.input-box input');
const chatMessages = document.querySelector('.chat-messages');

// Add event listener to create a new chat
newChatButton.addEventListener('click', () => {
  const newChat = document.createElement('div');
  newChat.classList.add('chat');
  newChat.textContent = `New Chat ${existingChats.children.length + 1}`;
  existingChats.appendChild(newChat);
});

// Add event listener to send a message
inputBox.addEventListener('keypress', (event) => {
  if (event.key === 'Enter' && inputBox.value.trim() !== '') {
    const message = document.createElement('div');
    message.classList.add('message');
    message.textContent = inputBox.value;
    chatMessages.appendChild(message);
    inputBox.value = '';
  }
// ... previous JS code ...

// Add event listener to toggle left pane visibility using the icon button
const iconButton = document.querySelector('.icon-button');
const leftPane = document.querySelector('.left-pane');

iconButton.addEventListener('click', () => {
  leftPane.classList.toggle('hidden');
});
